﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BattleShip
{
    public class Game
    {
        internal const int HIT=1;
        internal const int EMPTY = 0;
        internal const int NON = 2;
        internal const int SHIP = 3;
        internal const int DEATH = 5;

        internal const string NOT_ACTIVE = "Not active";
        internal const string CONNECTED = "Connected";
        internal const string READY = "Ready(%s)";
        internal const string WAIT_FOR_OPPONENT = "Waiting Enemy";
        internal const string ATTACK = "Attack!!!";
        internal const string WAIT_FOR_HIT = "Hope I hit?";
        internal const string WAIT_FOR_ATTACK = "Waiting Attack";
        internal const string GAME_OVER = "Game over";
        internal const string CREATING_MAP = "Placing ships";


        string current_game_state = NOT_ACTIVE;
        public bool game_over = false;
        public bool isConnected=false;
        Board myBoard, enemysBoard;
        int[,] me, enemy;
        int currentX=0, currentY=0;
        int myShipsCount = 0;
        string myIdforServer;
        Form1 parent;

        public Game(Form1 parent)
        {
            this.parent = parent;
            myBoard = new Board(this,"My Board");
            enemysBoard = new Board(this,"Enemy's Board");
            me = initNewIntBoard();
            enemy = initNewIntBoard();
        }

        public void initNewGame()
        {
            BatRequest req = new BatRequest(this);
            req.initNewGame();
        }

        int[,] initNewIntBoard()
        {
            int[,] board = new int[10, 10];
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                    board[i, j] = EMPTY;
            return board;
        }

        public void ReadyForGame()
        {
            BatRequest req = new BatRequest(this);
            req.iAmReady();
            generateMyBoard();
            showBoards();
        }

        public void showBoards()
        {
            myBoard.Show();
            enemysBoard.Show();
        }
        
        public void generateMyBoard()
        {
            Random rnd = new Random();
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                    if (rnd.Next(Math.Abs((int)DateTime.Now.Ticks)) % 10 > 8)
                    {
                        me[i, j] = SHIP;
                        myBoard.fight(i, j, SHIP);
                        myShipsCount++;
                        //if (myShipsCount == 10) return;
                    }
        }

        public void clickInBoard(int x, int y, Board board)
        {  
            if (board == enemysBoard)
            {
                if(current_game_state==ATTACK)
                    if (enemy[x, y] == EMPTY)
                    {
                        Server.attack(myIdforServer, x, y);
                        currentX = x;
                        currentY = y;
                        BatRequest req = new BatRequest(this);
                        req.getHitPlace(myIdforServer);
                        GameStateChanged(WAIT_FOR_HIT);
                    }
            }
        }

        void waitForAttack()
        {
            BatRequest req = new BatRequest(this);
            req.waitForAttack(myIdforServer);
            GameStateChanged(WAIT_FOR_ATTACK);
        }

        internal void response_for_Ready(string result)
        {
            myIdforServer = result;
            myBoard.Text = "My Board(" + result + ")";
            enemysBoard.Text = "Enemy's Board(" + result + ")";
            if (myIdforServer.Equals("second"))
            {
                waitForAttack();
                showBoards();
            }
            else if (myIdforServer.Equals("first"))
            {
                BatRequest req = new BatRequest(this);
                req.waitForTwoClients();
                GameStateChanged(WAIT_FOR_OPPONENT);
            }
            else
            {
                GameStateChanged(NOT_ACTIVE);
                MessageBox.Show("Too many connected clients. Closing..");
                parent.Close();
            }
        }

        internal void response_two_clients()
        {
            GameStateChanged(ATTACK);
        }
        
        internal void response_for_getHitPlace(string result)
        {
            int STATE;
            try
            {
                STATE = Int32.Parse(result);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                GameStateChanged(NOT_ACTIVE);
                return;
            }
            iAttackedHim(STATE);
        }

        private void iAttackedHim(int STATE)
        {
            if (STATE == DEATH)
            {
                enemysBoard.showText("Dead");
                GameStateChanged(GAME_OVER);
            }
            else
            {
                enemy[currentX, currentY] = STATE;
                enemysBoard.fight(currentX, currentY, STATE);
                enemysBoard.showText(STATE == HIT ? "Hit!" : "Miss");
                waitForAttack();
            }
        }

        internal void response_for_waitForAttack(int coords)
        {            
            int y = coords % 10;
            int x = coords / 10;
            int hit = 0;
            if (me[x, y] == EMPTY)
            {
                hit = NON;
                me[x, y] = NON;
                myBoard.fight(x, y, NON);
                myBoard.showText("Miss");
            }
            if (me[x, y] == SHIP)
            {
                myShipsCount--;
                hit = myShipsCount==0?DEATH:HIT;
                me[x, y] = HIT;
                myBoard.fight(x, y, HIT);
                myBoard.showText("Hit!");
            }
            GameStateChanged(ATTACK);
            BatRequest req = new BatRequest(this);
            req.iWasAttacked(myIdforServer, hit);
        }

        internal void reponse_for_initNewGame(bool result)
        {
            if (result)                
                GameStateChanged(CONNECTED);
            else
                GameStateChanged(NOT_ACTIVE);
        }

        private void GameStateChanged(string STATE)
        {
            current_game_state = STATE;
            switch (STATE)
            {
                case NOT_ACTIVE:
                    MessageBox.Show("Couldn't connect to server");
                    parent.changeLabel(STATE);
                    break;     
                case ATTACK:
                    if (myShipsCount == 0)
                    {
                        parent.changeLabel(GAME_OVER);
                        myBoard.showText("Death! You lose");
                    }
                    else
                        parent.changeLabel(STATE);
                    break;
                default:
                    parent.changeLabel(STATE);
                    break;
            }            
        }
    }
}
