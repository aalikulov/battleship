﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Resources;

namespace BattleShip
{
    public partial class Board : Form
    {
        Game parent;
        System.Timers.Timer timer = new System.Timers.Timer();

        public Board(Game game, string title)
        {
            InitializeComponent();
            Text = title;
            parent = game;
            dataGridView1.RowCount = 10;
            dataGridView1.ColumnCount = 10;
            dataGridView1.RowHeadersWidth = 45;
            dataGridView1.AllowUserToResizeRows = false;
            dataGridView1.AllowUserToResizeColumns = false;
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            for (int i = 0; i < 10; i++)
            {
                dataGridView1.Rows[i].Height = 20;
                dataGridView1.Columns[i].Width = 20;
                dataGridView1.Rows[i].HeaderCell.Value = i.ToString();
                dataGridView1.Columns[i].HeaderCell.Value = i.ToString();
                for (int j = 0; j < 10; j++)
                {
                    DataGridViewImageCell cell = new DataGridViewImageCell();
                    cell.Value = new Bitmap(BattleShip.Properties.Resources.empty);
                    dataGridView1[i, j] = cell;
                }
            }
            FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex>=0&&e.ColumnIndex>=0)
                parent.clickInBoard(e.RowIndex, e.ColumnIndex, this);
        }

        public void fight(int x,int y, int type)
        {
            try
            {
                DataGridViewImageCell cell = new DataGridViewImageCell();
                cell.Value = new Bitmap(type==Game.EMPTY?BattleShip.Properties.Resources.empty:
                                        type==Game.HIT?BattleShip.Properties.Resources.hit:
                                        type==Game.NON?BattleShip.Properties.Resources.non:
                                        BattleShip.Properties.Resources.ship);
                dataGridView1.Rows[x].Cells[y] = cell;
            }
            catch (Exception exc)
            {
                //nothing serious )
            }
        }

        public void showText(string text)
        {
            timer = new System.Timers.Timer(3000);
            timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Tick);
            timer.Start();
            label1.Text = text;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            label1.Text = "";
        }
    }
}
