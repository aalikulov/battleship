﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace BattleShip
{
    /// <summary>
    /// BattleShip http request. For using every method create another instance of class.
    /// </summary>
    class BatRequest
    {
        Game delegat;
        Thread thread;

        public BatRequest(Game parent)
        {
            delegat = parent;
        }

        private bool sleep(int msec)
        {
            Thread.Sleep(msec);
            return delegat.game_over;
        }

        //***********************************************************
        public void iAmReady()
        {
            thread = new Thread(_iAmReady);
            thread.Start();
        }

        private void _iAmReady()
        {
            string result = "false";
            while ((result.Equals("false") || result.Length > 10))
            {
                result = Server.iAmReady();
                if (sleep(50)) break;
            }
            delegat.response_for_Ready(result);
        }
        //***********************************************************
        public void waitForTwoClients()
        {
            Thread thread = new Thread(_waitForTwoClients);
            thread.Start();
        }

        private void _waitForTwoClients()
        {
            int count = 0;
            while (count != 2)
            {
                count = Server.clientsCount();
                if (sleep(50)) break;
            }
            delegat.response_two_clients();
        }
        //***********************************************************
        public void getHitPlace(string client)
        {
            Thread thread = new Thread(_getHitPlace);
            thread.Start(client);
        }

        private void _getHitPlace(object client)
        {
            string result = "false";
            while (result.Equals("false"))
            {
                result = Server.getHitPlace((string)client);
                if (sleep(50)) break;
            }
            delegat.response_for_getHitPlace(result);
        }
        //***********************************************************
        public void waitForAttack(string client)
        {
            Thread thread = new Thread(_waitForAttack);
            thread.Start(client);
        }

        private void _waitForAttack(object client)
        {
            string result = "false";
            while (result.Equals("false"))
            {
                result = Server.waitForAttack((string)client);
                if (sleep(50)) break;
            }
            try
            {
                delegat.response_for_waitForAttack(int.Parse(result));
            }
            catch { }
        }

        //***********************************************************
        public void initNewGame( )
        {
            Thread thread = new Thread(_initNewGame);
            thread.Start();
        }

        private void _initNewGame()
        {
            bool result = Server.initGame();
            delegat.reponse_for_initNewGame(result);
        }
        //***********************************************************
        public void iWasAttacked(String client, int hit)
        {
            Thread thread = new Thread(_iWasAttacked);
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("client", client);
            param.Add("hit", hit);
            thread.Start(param);
        }

        private void _iWasAttacked(object param)
        {
            Server.iWasAttacked((string)(((Dictionary<string, object>)param)["client"]), (int)(((Dictionary<string, object>)param)["hit"]));
            //delegat.reponse_for_iWasAttacked();
        }
    }
}
