﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace BattleShip
{
    class Server
    {
        const string server = "http://www.andromeda712.eu5.org";

        public static string iAmReady()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(server+"?func=ready");
            request.KeepAlive = true;
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream());
            return sr.ReadToEnd();
        }

        public static bool initGame()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(server + "?func=new_game");
            request.KeepAlive = true;
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream());
            return sr.ReadToEnd().Contains("true");
        }

        public static string attack(string client, int x, int y)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(server + "?func=attack&client=" + client + "&x=" + x.ToString() + "&y=" + y.ToString());
            request.KeepAlive = true;
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream());
            return sr.ReadToEnd();
        }

        public static string waitForAttack(string client)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(server + "?func=wait_attack&client=" + client);
            request.KeepAlive = true;
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream());
            return sr.ReadToEnd();
        }

        public static string getHitPlace(string client)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(server + "?func=get_hit_place&client=" + client);
            request.KeepAlive = true;
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream());
            return sr.ReadToEnd();
        }

        public static void iWasAttacked(string client, int hit)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(server + "?func=was_attacked&client=" + client + "&hit=" + hit.ToString());
            request.KeepAlive = true;
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream());
           // return sr.ReadToEnd();
        }

        public static int clientsCount()
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(server + "?func=client_count");
                request.KeepAlive = true;
                request.Method = "GET";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader sr = new StreamReader(response.GetResponseStream());
                return Int32.Parse(sr.ReadToEnd());
            }
            catch(Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Ошибка."+e.Message);
                return 0;
            }
        }
    }
}
