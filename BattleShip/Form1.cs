﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BattleShip
{
    public partial class Form1 : Form
    {
        Game myGame;
        public Form1()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            myGame = new Game(this);
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myGame.ReadyForGame();
        }

        private void initServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myGame.initNewGame();
        }

        public void changeLabel(string text)
        {
            Font tmp=new Font(label1.Font.FontFamily,1);;
            label1.Text = text;
            label1.Font = tmp;
            int i = 1;
            while (label1.Width < 236)
            {
                tmp = new Font(label1.Font.FontFamily,i++);
                label1.Font =tmp;               
                label1.Top = (70 - label1.Height) / 2+24;
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            myGame.game_over = true;
        }
    }
}
